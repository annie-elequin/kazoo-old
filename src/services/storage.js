import {AsyncStorage} from 'react-native';

export const saveUserDataToDevice = (data) => {
  AsyncStorage.setItem('userData', JSON.stringify(data));
};

export const getUserDataFromDevice = async () => {
  try {
    const data = await AsyncStorage.getItem('userData');
    return JSON.parse(data);
  } catch (e) {
    // console.log('No user data on device.');
  }
};

export const saveIrisKeypairToDevice = (keypair) => {
  if (keypair) {
    AsyncStorage.setItem('irisKeypair', JSON.stringify(keypair));
  }
}

export const getIrisKeypairFromDevice = async () => {
  try {
    const data = await AsyncStorage.getItem('irisKeypair');
    return JSON.parse(data);
  } catch (e) {
    console.warn('No Iris keypair on device.');
    return null;
  }
};