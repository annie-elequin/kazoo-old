import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ChatListScreen from "../scenes/chatList/ChatListScreen";
import ChatScreen from "../scenes/chat/ChatScreen";

const Stack = createStackNavigator();

export default function RootStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ChatList"
        component={ChatListScreen}
        options={{ title: 'Chats' }}
      />
      <Stack.Screen
        name="Chat"
        component={ChatScreen}
        options={({route}) => ({ title: route.params.title })}
      />
    </Stack.Navigator>
  );
}
