/* eslint-disable react/display-name */
import React, { useContext, useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './SplashScreen';
import { AuthContext } from '../contexts/AuthContext';
import { getUserDataFromDevice, getIrisKeypairFromDevice } from '../services/storage';
import RootStack from './RootStack';
import AuthScreen from '../scenes/auth/AuthScreen';
import { login as irisLogin, isValidKey } from '../services/IrisService';
import gunService from '../services/GunService';
import { Chat } from 'iris-lib'

const Stack = createStackNavigator();

export default function AppNavigator() {
  const {userKeypair, setKeypair} = useContext(AuthContext);
  const [isLoadingAuth, setIsLoadingAuth] = useState(false);

  const logInWithKey = (key) => {
    gunService.user().auth(key)
    irisLogin(gunService, key)
    setKeypair(key)
    Chat.setOnline(gunService, true)
  }

  useEffect(() => {
      setIsLoadingAuth(true)
      getIrisKeypairFromDevice().then(keypair => {
        if (keypair) {
          try {
            let key = typeof keypair === 'object' ? keypair : JSON.parse(keypair)
            if (isValidKey(key)) {
              logInWithKey(key)
            }
          } catch (e) {
            // todo: error handling
            console.warn(e)
          }
        }
        setIsLoadingAuth(false)
      })
  }, []);

  return (
    <Stack.Navigator>
      {!isLoadingAuth ? (
        userKeypair ? (
          <Stack.Screen name="Root" component={RootStack} options={{headerShown: false}} />
          ) : (
          <Stack.Screen name="Auth" component={AuthScreen} options={{title: 'Welcome to Kazoo'}} />
        )
      ) : (
          <Stack.Screen name='Splash' component={SplashScreen} options={{title: 'Splash'}} />
      )}
    
    </Stack.Navigator>
  );
}