import React from 'react';
import { createContext, useState } from 'react';
import { saveUserDataToDevice, saveIrisKeypairToDevice } from '../services/storage';

const initialAuthContext = {
  userKeypair: null
};

export const AuthContext = createContext(initialAuthContext);

export default function AuthContextProvider({children}) {
  const [authContext, setAuthContext] = useState(initialAuthContext);

  const setKeypair = key => {
    saveIrisKeypairToDevice(key)
    setAuthContext({...authContext, userKeypair: key});
  };

  return (
    <AuthContext.Provider value={{...authContext, setKeypair}}>
      {children}
    </AuthContext.Provider>
  );
}