import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, useTheme} from '@ui-kitten/components'

export default function Message({message}) {
  const theme = useTheme()
  // console.log(message)
  return (
    <View style={[styles.bubble, message._info.selfAuthored ? {alignSelf: 'flex-end', backgroundColor: theme['color-primary-500']} : {}]}>
      <Text category='s1'>{message.text}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  bubble: {
    backgroundColor: '#343434',
    maxWidth: 350,
    paddingVertical: 10,
    paddingHorizontal: 16,
    marginBottom: 4,
    borderRadius: 18,
    alignSelf: 'flex-start'
  }
})