import React, { useEffect, useState } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { Text, Button } from '@ui-kitten/components';
import { Chat, util } from 'iris-lib'
import gunService from '../../services/GunService';
import { session } from '../../services/IrisService';
import Message from './components/Message';

let initMessages = []

export default function ChatScreen({navigation, route}) {
  const [messages, setMessages] = useState([])
  const [onlineStatus, setOnlineStatus] = useState({})
  const [lastSeenTime, setLastSeenTime] = useState(null)

  const [chatService, setChatService] = useState(null)



  const pub = route.params.pub
  let thisName = null

  const setName = (name) => {
    thisName = name
    // navigation.setParams({ title: name })

    // todo: why set user id "2" to the name..?
    const newMessages = [...messages]
    newMessages.forEach(m => {
      if (m.user._id === 2) {
        m.user.name = name
      }
    })
  }

  const onMessage = (msg, info) => {
    console.log('on message', msg)
    chatService && chatService.setMyMsgsLastSeenTime()

    msg.sent = msg.time <= (onlineStatus && onlineStatus.lastActive)
    msg.received = msg.time <= lastSeenTime
    msg.createdAt = new Date(msg.time)
    msg._id = msg.time + (info.selfAuthored ? 0 : 1)
    msg.user = {
      name: thisName || msg.author,
      _id: (info.selfAuthored ? 1 : 2),
    }

    if (initMessages.find(m => m._id === msg._id) || messages.find(m => m._id === msg._id)) {
      return;
    }

    initMessages = [...initMessages, msg]
    let newMessages = [...initMessages]
    setMessages(newMessages.sort((a, b) => b.createdAt - a.createdAt))
  }

  const sendPracticeMessage = (messages = []) =>{
    chatService.send({
      text: 'hey',
      time: new Date().toISOString(),
      author: 'me',
    })
    // messages.forEach((m) => {
    //   chat.send({
    //     text: m.text,
    //     time: new Date().toISOString(),
    //     author: 'me',
    //   })
    // })
  }

  useEffect(() => {
    setChatService(null)
    setMessages([])
    setName('')
    gunService.user(pub).get('profile').get('name').on(setName)

    const chat = new Chat({gun: gunService, key: session.keypair, participants: pub, onMessage});
 
    chat.setMyMsgsLastSeenTime()
    chat.getTyping(isTyping => {
      console.log('is typing: ', isTyping)
      // navigation.setParams({isTyping})
    })
    chat.getTheirMsgsLastSeenTime(lastSeenTime => {
      const newMessages = [...messages]
      newMessages.forEach(m => {
        m.received = m.sent = (m.time <= lastSeenTime)
      })
      setMessages(newMessages)
    })
    Chat.getOnline(gunService, pub, onlineStatus => {
      // navigation.setParams({onlineStatus})
      setOnlineStatus(onlineStatus)
    })
    setChatService(chat)

    return () => {
      initMessages = []
    }
  }, [])

  useEffect(() => {
    console.log(chatService)
  }, [chatService])

  return (
    <View>
      <FlatList
        inverted
        style={styles.flatList}
        data={messages}
        renderItem={({item}) => (
          <Message message={item} />
        )}
      />
      {/* {messages.map(m => (
        <Text>{m.text}</Text>
      ))} */}
      <Button onPress={sendPracticeMessage}>Send</Button>
    </View>
  )
}

const styles = StyleSheet.create({
  flatList: {
    paddingVertical: 12,
    paddingHorizontal: 6
  }
})

// import React from 'react'
// import { TouchableOpacity, Text, View } from 'react-native'
// import { GiftedChat } from 'react-native-gifted-chat'
// import { Chat, util } from 'iris-lib'
// import Style from './Style'
// import Identicon from 'App/Components/Identicon'
// import NavigationService from 'App/Services/NavigationService'
// import gun from 'App/Services/GunService'
// import { session } from 'App/Services/IrisService'

// class ChatScreenn extends React.Component {
//   state = {
//     messages: [],
//     onlineStatus: {},
//   }

//   static navigationOptions = ({navigation}) => {
//     const {state} = navigation;
//     return {
//       headerTitle: (
//         <TouchableOpacity style={Style.headerLeft} onPress={() => NavigationService.navigate('ContactScreen', { pub: state.params.pub, title: state.params.title || '' })}>
//           <Identicon pub={state.params.pub} width={40} style={Style.headerIdenticon} />
//           <View>
//             <Text>{state.params.title || ''}</Text>
//             <Text style={Style.lastActive}>{(state.params.isTyping && 'typing...') || state.params.onlineStatus && (state.params.onlineStatus.isOnline && 'online' || 'last active ' + util.formatDate(new Date(state.params.onlineStatus.lastActive)))}</Text>
//           </View>
//         </TouchableOpacity>
//       )
//     }
//   }

//   componentDidMount() {
//     const pub = this.props.navigation.getParam('pub')
//     const setName = name => {
//       this.name = name
//       this.props.navigation.setParams({title: name})
//       this.setState(previousState => {
//         const newState = {...previousState}
//         for (let i = 0; i < newState.messages.length; i++) {
//           const m = newState.messages[i]
//           if (m.user._id === 2) {
//             m.user.name = name
//           }
//         }
//         return newState
//       })
//     }
//     setName('')
//     gun.user(pub).get('profile').get('name').on(setName)
//     const onMessage = (msg, info) => {
//       this.chat.setMyMsgsLastSeenTime()
//       this.setState(previousState => {
//         msg.sent = msg.time <= (previousState.onlineStatus && previousState.onlineStatus.lastActive)
//         msg.received = msg.time <= previousState.lastSeenTime
//         msg.createdAt = new Date(msg.time)
//         msg._id = msg.time + (info.selfAuthored ? 0 : 1)
//         msg.user = {
//           name: this.name || msg.author,
//           _id: (info.selfAuthored ? 1 : 2),
//         }
//         newMessages = previousState.messages.concat(msg)
//         newMessages.sort((a, b) => b.createdAt - a.createdAt)
//         return { ...previousState, messages: newMessages }
//       });
//     }
//     this.chat = new Chat({gun, key: session.keypair, participants: pub, onMessage});
//     this.chat.setMyMsgsLastSeenTime()
//     this.chat.getTyping(isTyping => {
//       this.props.navigation.setParams({isTyping})
//     })
//     this.chat.getTheirMsgsLastSeenTime(lastSeenTime => {
//       this.setState(previousState => {
//         const newMessages = [...previousState.messages]
//         newMessages.forEach(m => m.received = m.sent = (m.time <= lastSeenTime))
//         return { ...previousState, messages: newMessages, lastSeenTime }
//       })
//     })
//     Chat.getOnline(gun, pub, onlineStatus => {
//       this.props.navigation.setParams({onlineStatus})
//       this.setState(previousState => {
//         return {...previousState, onlineStatus}
//       })
//     })
//   }

//   onSend(messages = []) {
//     messages.forEach(m => {
//       this.chat.send({
//         text: m.text,
//         time: new Date().toISOString(),
//         author: 'me'
//       })
//     })
//   }

//   render() { // backgroundColor: '#e5ddd5'
//     return (
//       <View style={{backgroundColor: '#ffffff', flex: 1}}>
//         <GiftedChat
//           messages={this.state.messages}
//           renderAvatar={null}
//           onSend={messages => this.onSend(messages)}
//           user={{
//             _id: 1,
//           }}
//           onInputTextChanged={text => {
//             this.chat.setTyping(text.length > 0)
//           }}
//         />
//       </View>
//     )
//   }
// }
