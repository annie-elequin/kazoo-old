import React, { Component, useEffect, useState } from 'react';
import { Image, View, StyleSheet } from 'react-native';
import gunService from '../../../services/GunService';

export default function ChatListAvatar({pub}) {
  const [photo, setPhoto] = useState(null)

  useEffect(() => {
    gunService.user(pub).get('profile').get('photo').on(setPhoto)
  }, [])

  return (
    <View style={styles.wrapper}>
      {photo && (
        <Image
          style={styles.photo}
          resizeMode='cover'
          source={{uri: photo}}
        />
      )}
    </View>
  )
}


const styles = StyleSheet.create({
  wrapper: {
    width: 50,
    height: 50,
    backgroundColor: 'gray',
    borderRadius: 50,
    marginRight: 10,
    overflow: 'hidden',
  },
  photo: {
    width: 50,
    height: 50
  }
})