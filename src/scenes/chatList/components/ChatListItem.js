import React, { Component, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { TouchableWithoutFeedback, StyleSheet, View, TouchableHighlight } from 'react-native';
// import Identicon from './Identicon'
import { util, Chat } from 'iris-lib'
import gunService from '../../../services/GunService';
import { Text } from '@ui-kitten/components';
// import Svg from 'App/Components/Svg'
// import { SvgXml } from 'react-native-svg'
import { SCREEN_WIDTH } from '../../../constants';
import {MaterialCommunityIcons} from '@expo/vector-icons'
import ChatListAvatar from './ChatListAvatar';
import { useNavigation } from '@react-navigation/native';

export default function ChatListItem({chat}) {
  const [isTyping, setIsTyping] = useState(false)
  const [lastSeenTime, setLastSeenTime] = useState(null)
  const [isOnline, setIsOnline] = useState(null)

  const {navigate} = useNavigation()

  const navToChatScreen = () => {
    // () => NavigationService.navigate('ChatScreen', { pub: chat.pub, title: chat.name })
    navigate('Chat', {title: chat.name || 'Chat', pub: chat.pub})
  }

  const renderLatestSeen = () => {
		if (chat.latest && chat.latest.info.selfAuthored && !isTyping) {
			const t = chat.latest.date.toISOString()
			if (lastSeenTime >= t) {
        return <MaterialCommunityIcons name='check-all' color='dodgerblue' size={18} />
			} else if (isOnline && isOnline.lastActive >= t) {
        return <MaterialCommunityIcons name='check' color='dodgerblue' size={18} />
			} else {
        return <MaterialCommunityIcons name='check' color='gray' size={18} />
			}
		} else return null
  }
  
  const initState = () => {
		chat.getTheirMsgsLastSeenTime(setLastSeenTime)
		Chat.getOnline(gunService, chat.pub, setIsOnline)
		chat.getTyping(setIsTyping)
  }

  useEffect(() => {
    initState()
  }, [])

  let latestTimeText = ''
  if (chat.latest) {
    latestTimeText = util.getDaySeparatorText(chat.latest.date, chat.latest.date.toLocaleDateString({dateStyle:'short'}))
    if (latestTimeText === 'today') { latestTimeText = util.formatTime(chat.latest.date) }
  }

  return (
    <TouchableHighlight onPress={navToChatScreen} underlayColor={'#111111'}>
      <View style={[styles.wrapper, chat.hasUnseen ? {backgroundColor: 'blue'} : {}]}>
        <ChatListAvatar pub={chat.pub} />
        <View>
          <View style={styles.textWrapper}>
            <Text category='s1' style={{marginRight: 8}}>{chat.name || ''}</Text>
            <Text category='c2' style={{opacity: .4}}>{latestTimeText}</Text>
          </View>
          <View style={styles.snippet}>
            {renderLatestSeen()}
            <Text category='s2' numberOfLines={1} style={styles.snippetText}>
              {(isTyping && 'Typing...') || (chat.latest && chat.latest.text) || ''}
            </Text>
          </View>
        </View>
      </View>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    height: 66,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8
  },
  textWrapper: {
    flexDirection: 'row', 
    alignItems: 'center'
  },
  snippet: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  snippetText: {
    width: SCREEN_WIDTH - 140,
    opacity: .7,
    marginLeft: 4
  }
})