import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Input, Button } from '@ui-kitten/components';

export default function LoginForm({logInWithKey}) {

  return (
    <>
      <Input
        size='large'
        style={styles.radius}
        autoCorrect={false}
        editable
        autoFocus
        placeholder="Paste your private key"
        placeholderTextColor="white"
        selectionColor="white"
        onChangeText={logInWithKey}
      />
      <Text appearance='hint' style={{marginVertical: 18}}>or</Text>
      <Button style={[styles.radius, {width: '100%'}]} onPress={() => {}}>Scan QR</Button>
    </>
  )
}


const styles = StyleSheet.create({
  radius: {
    borderRadius: 12,
  },
})