import React, { useState, useContext } from 'react';
import { View, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { Button } from '@ui-kitten/components';
import SignupForm from './components/SignupForm';
import LoginForm from './components/LoginForm';
import { Key, Chat } from 'iris-lib'
import {login as irisLogin} from '../../services/IrisService';
import gunService from '../../services/GunService';
import { AuthContext } from '../../contexts/AuthContext';

export default function AuthScreen() {
  const [showSignup, setShowSignup] = useState(true)
  const [isLoading, setIsLoading] = useState(false)

  const {setKeypair} = useContext(AuthContext)

  const toggleView = () => setShowSignup(!showSignup)

  const logInWithKey = (key) => {
    console.log(key)
    gunService.user().auth(key)
    irisLogin(gunService, key)
    console.log('logged in')
    setKeypair(key)
    Chat.setOnline(gunService, true)
  }

  const logInAsNewUser = (name) => {
    if (!(name && name.length > 0)) {
      return; // TODO: show error
    }
    if (name.indexOf('{') !== -1 || name.indexOf('}') !== -1) {
      return; // prevent accidentally pasting private key here
    }
    Key.generate().then(key => {
      logInWithKey(key, name)
      Chat.createChatLink(gunService, key)
    })
  }

  return isLoading ? (<View style={styles.container}></View>) : (
    <KeyboardAvoidingView style={styles.container} behavior="height">
        {showSignup ? (
          <SignupForm logInAsNewUser={logInAsNewUser} />
        ) : (
          <LoginForm logInWithKey={logInWithKey} />
        )}
        <View>
          <Button appearance='ghost' style={{marginTop: 24}} onPress={toggleView}>
            {showSignup ? 'Already signed up?' : 'Need an account?'}
          </Button>
        </View>
    </KeyboardAvoidingView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 24
  },
  radius: {
    borderRadius: 12,
  }
})